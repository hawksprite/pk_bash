﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackdropScapeYSetter : MonoBehaviour {

    RectTransform r;

    void Awake()
    {
        r = GetComponent<RectTransform>();
    }

    void Update()
    {
        // Keep setting top to Y
        r.position = new Vector3(r.position.x, Screen.height / 2, -10.0f);
    }
}
