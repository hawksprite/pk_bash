﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackdropScapeManager : MonoBehaviour {

    public GameObject canvasRoot;
    public GameObject scapePrefab;

    public int maxScapeSize;
    public float scapeWidth;
    public float scapeSpeed = 100.0f;

    private List<RectTransform> scapeIndex = new List<RectTransform>();

	void Update () {
	    // If we're short a scape add one
        if (scapeIndex.Count < maxScapeSize)
        {
            SpawnAnotherScape();
        }

        // Shift all of the current scapes
        for (int i = 0; i < scapeIndex.Count; i++)
        {
            Vector3 c = scapeIndex[i].position;
            c.x -= Time.smoothDeltaTime * scapeSpeed;
            scapeIndex[i].position = c;
        }

        // See if the left-most scape needs to be dropped yet
        if (scapeIndex[0].position.x < ((scapeWidth + 100) * -1.0f))
        {
            // Drop the lead scape
            GameObject.Destroy(scapeIndex[0].gameObject);
            scapeIndex.RemoveAt(0);
        }
	}

    public void SpawnAnotherScape()
    {
        Vector2 newRoot = new Vector2();

        // If there are other scapes calculate the new root X location
        if (scapeIndex.Count != 0)
        {
            newRoot.x = scapeIndex[scapeIndex.Count - 1].GetComponent<RectTransform>().position.x + scapeWidth;
        }

        // Spawn a new one
        GameObject g = (GameObject)Instantiate(scapePrefab);
        g.transform.parent = canvasRoot.transform;
        RectTransform r = g.GetComponent<RectTransform>();
        r.position = new Vector3(newRoot.x, 0, r.position.z);
        r.sizeDelta = new Vector2(400, 0);
        r.SetAsFirstSibling();
        scapeIndex.Add(r);
    }
}
