﻿using UnityEngine;
using System.Collections;
using System;

public class UiSparkanimationImage : MonoBehaviour {

    // Animation configuration variables
    public int duration = 400; // In milliseconds
    public float moveSpeed = 440.0f;
    public float scaleSpread = 0.2f; // The size of the spread off of 1.0 you can go for scale.  So for 0.2 it's a possibility of 0.8 to 1.2 for the scale.
    public float scale = 1.0f;
    public float rotateSpeed = 200.0f;

    // Vars for actually handling the animation
    DateTime spawnTime;
    RectTransform r;
    Vector2 velocity = Vector2.zero;
    Vector2 startingTarget;
    bool hasStartingTarget = false;
    bool hasRecievedTarget = false;
    float rotationZ = 0;

	// Use this for initialization
	void Start () {
        spawnTime = DateTime.Now;
        r = GetComponent<RectTransform>();
        velocity = UnityEngine.Random.insideUnitCircle;

        duration += UnityEngine.Random.Range(-200, 200); // Give the possibility of a duration offset of 200+/- milliseconds.
        scale = UnityEngine.Random.Range(scale - scaleSpread, scale + scaleSpread);
	}

    public void SetPosition(Vector2 t)
    {
        hasRecievedTarget = true;
        startingTarget = t;
        spawnTime = DateTime.Now;
    }
	
	// Update is called once per frame
	void Update () {
        if (hasRecievedTarget)
        {
            if (!hasStartingTarget) { 
                hasStartingTarget = true;

                // Apply the starting position
                Vector3 current = r.position; 
                current.x = startingTarget.x; 
                current.y = startingTarget.y;
                r.position = current;
            }

            // Kill it off after a specified duration
            if ((DateTime.Now - spawnTime).TotalMilliseconds >= duration)
            {
                GameObject.Destroy(this.gameObject);
            }

            // Move the sprite along it's randomly given velocity
            Vector3 c = r.position;

            c.x += velocity.x * Time.smoothDeltaTime * moveSpeed;
            c.y += velocity.y * Time.smoothDeltaTime * moveSpeed;

            // Use the change to update the rotation of the sprite
            //r.rotation = Quaternion.Euler(new Vector3(0, 0, rotationZ));

            r.position = c;

            scale = Mathf.Lerp(scale, 0.2f, Time.smoothDeltaTime);

            // Apply the scale
            r.localScale = new Vector3(scale, scale, 1.0f);
        }
	}
}
