﻿using UnityEngine;
using System.Collections;

public class SPA : MonoBehaviour {

    private static SPA _instance;

    public static SPA instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SPA>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }


    public AudioClip clip;
    public AudioClip attack;
    public AudioClip wrong;

    private AudioSource s;

	// Use this for initialization
	void Start () {
        s = GetComponent<AudioSource>();
	}

    public void PClip()
    {
        s.PlayOneShot(clip);
    }

    public void HClip()
    {
        s.PlayOneShot(attack);
    }

    public void WClip()
    {
        s.PlayOneShot(wrong);
    }
}
