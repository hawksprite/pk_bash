﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class G_TypeManager : MonoBehaviour {

    // For convience sake we're going to have 4 internal hooks to the needed scripts on our type buttons
    private G_TypeChoiceButton tcb1;
    private G_TypeChoiceButton tcb2;
    private G_TypeChoiceButton tcb3;
    private G_TypeChoiceButton tcb4;

    [HideInInspector]
    public int currentEnemyTID = 0;

    bool finishedFadingOutTrigger = true;
    public bool lastFadeOutFinished = false;

	void Start () {
	    // The GM will set these links on awake so we're safe to probe here
        tcb1 = G_GameManager.instance.typeSelectionObject1.GetComponent<G_TypeChoiceButton>();
        tcb2 = G_GameManager.instance.typeSelectionObject2.GetComponent<G_TypeChoiceButton>();
        tcb3 = G_GameManager.instance.typeSelectionObject3.GetComponent<G_TypeChoiceButton>();
        tcb4 = G_GameManager.instance.typeSelectionObject4.GetComponent<G_TypeChoiceButton>();
	}

	void Update () {
        if (!finishedFadingOutTrigger)
        {
            if (lastFadeOutFinished)
            {
                // Call for a full refresh
                RefreshTypes(false);
                finishedFadingOutTrigger = true;
                lastFadeOutFinished = false;
            }
        }
	}

    public void RefreshTypes(bool preAnimation)
    {
        if (preAnimation)
        {
            // Fade out all the buttons
            G_AnimationManager.instance.PlayFadeOutButton(1);
            G_AnimationManager.instance.PlayFadeOutButton(2);
            G_AnimationManager.instance.PlayFadeOutButton(3);
            G_AnimationManager.instance.PlayFadeOutButton(4);
            G_AnimationManager.instance.PlayFadeOutEnemyPanel();

            // Once they fade out it'll come back to the type manager and we'll do the actual refresh then fade in
            finishedFadingOutTrigger = false; // Go false so our update loop  hunts for a fade out finished 
            lastFadeOutFinished = false;
        }
        else
        {
            // Setup our current enemy
            currentEnemyTID = TypeLibrary.instance.RandomTID;
            G_GameManager.instance.enemyPanelObject.GetComponentInChildren<Text>().text = TypeLibrary.instance.Library[currentEnemyTID].Name;

            // Pull the color, apply red green and blue channels then push it back up
            //Color c = G_GameManager.instance.enemyPanelObject.GetComponent<Image>().color;
            //c.r = TypeLibrary.instance.Library[currentEnemyTID].baseColor.r;
            //c.g = TypeLibrary.instance.Library[currentEnemyTID].baseColor.g;
            //c.b = TypeLibrary.instance.Library[currentEnemyTID].baseColor.b;
            //G_GameManager.instance.enemyPanelObject.GetComponent<Image>().color = c;
            G_GameManager.instance.enemyPanelObject.GetComponent<G_EnemyPanel>().SetColor(TypeLibrary.instance.Library[currentEnemyTID].baseColor);

            G_GameManager.instance.enemyPanelTypeIcon.GetComponent<Image>().sprite = TypeLibrary.instance.Library[currentEnemyTID].sprite;


            // We need to pick 4 random ID's, one that'll be super effective and one that will be resisted
            List<int> choicePool = new List<int>();

            choicePool.Add(TypeLibrary.instance.RandomEffectiveAgainst(currentEnemyTID));
            choicePool.Add(TypeLibrary.instance.RandomWeakAgainst(currentEnemyTID));
            choicePool.Add(TypeLibrary.instance.RandomTID);
            choicePool.Add(TypeLibrary.instance.RandomTID);

            for (int i = 0; i < choicePool.Count; i++)
            {
                int t = choicePool[i];
                int ti = Random.Range(i, choicePool.Count);
                choicePool[i] = choicePool[ti];
                choicePool[ti] = t;
            }

            // Set them
            tcb1.SetupType(choicePool[0]);
            tcb2.SetupType(choicePool[1]);
            tcb3.SetupType(choicePool[2]);
            tcb4.SetupType(choicePool[3]);

            TypeLibrary.instance.ClearRandomHistory(); // Clear out our random history for the next round.

            
            G_AnimationManager.instance.PlayFadeInButton(1);
            G_AnimationManager.instance.PlayFadeInButton(2);
            G_AnimationManager.instance.PlayFadeInButton(3);
            G_AnimationManager.instance.PlayFadeInButton(4);
            G_AnimationManager.instance.PlayFadeInEnemyPanel();
        }
    }

    public bool CheckSelection(int tid)
    {
        // Choosen effectivness
        Effectivness chosenEffectivness = TypeLibrary.instance.Library[tid].CheckMatch(currentEnemyTID);

        bool blemish = true;
        // Now we need to check all 4 buttons, if any of them have a "better" effectivness.  Then it's a false.
        for (int i = 0; i < 4; i++)
        {
            switch (i)
            {
                case 0: if (!subCheckSelectionButton(tcb1, chosenEffectivness)) { blemish = false; } break;
                case 1: if (!subCheckSelectionButton(tcb2, chosenEffectivness)) { blemish = false; } break;
                case 2: if (!subCheckSelectionButton(tcb3, chosenEffectivness)) { blemish = false; } break;
                case 3: if (!subCheckSelectionButton(tcb4, chosenEffectivness)) { blemish = false; } break;
            }
        }

        // Go back and setup the button for the one we clicked


        return blemish;
    }

    private bool subCheckSelectionButton(G_TypeChoiceButton b, Effectivness compare)
    {
        // Pull it's effectivness
        Effectivness e = TypeLibrary.instance.Library[b.tid].CheckMatch(currentEnemyTID);

        // Update the button text to reflect it
        b.SetText(GlobalConfiguration.EffectivnessName(e));

        if ((int)e <= (int)compare){
            return true;
        }
        else{
            return false;
        }

    }
}
