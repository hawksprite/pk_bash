﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class G_EnemyPanel : MonoBehaviour {

    public float LerpSpeed = 10.0f;
    private Color target;
    private Image i;

	// Use this for initialization
	void Start () {
        i = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        Color top = i.color;

        Color fresh = Color.Lerp(top, target, Time.smoothDeltaTime * LerpSpeed);

        i.color = fresh;
	}

    public void SetColor(Color c)
    {
        target = c;
    }
}
