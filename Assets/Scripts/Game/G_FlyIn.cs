﻿using UnityEngine;
using System.Collections;
using System;

public class G_FlyIn : MonoBehaviour {

    // RectTransform for this button, and it's original position
    RectTransform r;
    Vector3 original;

    // Target of the enemy rect transform that we want to fly over and "hit"
    Vector3 target;

	// Use this for initialization
	void Start () {
        r = GetComponent<RectTransform>();
        original = r.position;

        target = G_GameManager.instance.enemyPanelObjectRect.position;
	}

    // Private vars for the animation
    bool shouldEndAnimation = false;
    DateTime shouldEndDelay;
	
	// Update is called once per frame
	void Update () {
	    // Lerp towards the target position, then we can "Flash" back home with the end animation

        if (shouldEndAnimation)
        {
            // Fly back to the original position
            if (Vector3.Distance(r.position, original) <= 1.0f)
            {
                EndAnimation();
            }

            r.position = Vector3.Lerp(r.position, original, GlobalConfiguration.animationFlyInSpeed * Time.smoothDeltaTime);
        }
        else
        {
            // Lerpen in one old animation movement
            r.position = Vector3.Lerp(r.position, target, GlobalConfiguration.animationFlyInSpeed * Time.smoothDeltaTime);

            if (Vector3.Distance(r.position, target) <= 40.0f)
            {
                playHitEffects(); // Pop them before the transition back
            }

            if (Vector3.Distance(r.position, target) <= 15.0f)
            {
                shouldEndAnimation = true;
            }
        }
	}


    bool hasPlayed = false;
    void playHitEffects()
    {
        if (!hasPlayed)
        {
            hasPlayed = true;

            // When we hit shake the enemy pane and let out some particles
            // Also play laser on the enemy to show that we hit 'em hard!
            RectTransform enemy = G_GameManager.instance.enemyPanelObject.GetComponent<RectTransform>();
            G_AnimationManager.instance.PlayLaserAnimation(new Vector2(enemy.position.x, enemy.position.y), 40);

            // Check for phone "Vibration" on success
            if (GlobalConfiguration.usePhoneVibration)
            {
                //Handheld.Vibrate();
            }

            // Enemy panel "Hit" shake
            G_AnimationManager.instance.PlayEnemyPanelShake(350);
        }
    }

    void EndAnimation()
    {
        // Fall back to the original location and disable this script
        r.position = original;
        Destroy(this);
    }
}
