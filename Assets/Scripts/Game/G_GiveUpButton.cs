﻿using UnityEngine;
using System.Collections;

public class G_GiveUpButton : MonoBehaviour {

    public void Button()
    {
        G_GameManager.instance.GameOver(false); // Call a game over, no need for a game over screen
    }
}
