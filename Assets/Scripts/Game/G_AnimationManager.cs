﻿using UnityEngine;
using System.Collections;
using System;

public class G_AnimationManager : MonoBehaviour {

    // Handle Animations and other graphical helpers here

    // Vars for the shake animation
    private bool playingShakeAnimation = false;
    private DateTime playingShakeAnimationStart;
    private int playingShakeAnimationDuration = 0;

	 //Here is a private reference only this class can access
    private static G_AnimationManager _instance;
 
    //This is the public reference that other classes will use
    public static G_AnimationManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if(_instance == null)
                _instance = GameObject.FindObjectOfType<G_AnimationManager>();
            return _instance;
        }
    }

    void Start()
    {

    }

    void Update()
    {
        // Handle updating animations

        if (playingShakeAnimation || G_GameManager.instance.isGameOver)
        {
            if ((DateTime.Now - playingShakeAnimationStart).TotalMilliseconds >= playingShakeAnimationDuration)
            {
                playingShakeAnimation = false; // Should be good
            }

            // "Shake" the enemy panel
            Vector3 shakenLoc = G_GameManager.instance.storedEnemyPanelObjectPosition;
            Vector2 rndShake = UnityEngine.Random.insideUnitCircle * GlobalConfiguration.enemyPanelShakeIntensity;

            // Apply the shaken changes
            shakenLoc.x += rndShake.x;
            shakenLoc.y += rndShake.y;

            // Reapply to the transform
            G_GameManager.instance.enemyPanelObjectRect.position = shakenLoc;
        }
        else
        {
            // Make sure it doesn't bug off a shake and keep it's position locked down
            G_GameManager.instance.enemyPanelObjectRect.position = G_GameManager.instance.storedEnemyPanelObjectPosition;
        }
    }

    // Button fly in animation
    public void PlayButtonFlyIn(int id)
    {
        switch (id)
        {
            case 1: G_GameManager.instance.typeSelectionObject1.AddComponent<G_FlyIn>(); break;
            case 2: G_GameManager.instance.typeSelectionObject2.AddComponent<G_FlyIn>(); break;
            case 3: G_GameManager.instance.typeSelectionObject3.AddComponent<G_FlyIn>(); break;
            case 4: G_GameManager.instance.typeSelectionObject4.AddComponent<G_FlyIn>(); break;
        }
    }

    // Fade button out
    public void PlayFadeOutButton(int id)
    {
        switch (id)
        {
            case 1: G_GameManager.instance.typeSelectionObject1.AddComponent<G_FadeOut>(); break;
            case 2: G_GameManager.instance.typeSelectionObject2.AddComponent<G_FadeOut>(); break;
            case 3: G_GameManager.instance.typeSelectionObject3.AddComponent<G_FadeOut>(); break;
            case 4: G_GameManager.instance.typeSelectionObject4.AddComponent<G_FadeOut>(); break;
        }
    }

    public void PlayFadeInButton(int id)
    {
        switch (id)
        {
            case 1: G_GameManager.instance.typeSelectionObject1.AddComponent<G_FadeIn>(); break;
            case 2: G_GameManager.instance.typeSelectionObject2.AddComponent<G_FadeIn>(); break;
            case 3: G_GameManager.instance.typeSelectionObject3.AddComponent<G_FadeIn>(); break;
            case 4: G_GameManager.instance.typeSelectionObject4.AddComponent<G_FadeIn>(); break;
        }
    }


    // Fade enemy panel
    public void PlayFadeOutEnemyPanel()
    {
        //G_GameManager.instance.enemyPanelObject.AddComponent<G_FadeOut>();
        //G_GameManager.instance.enemyPanelTypeIcon.AddComponent<G_FadeOut>();
    }

    public void PlayFadeInEnemyPanel()
    {
        //G_GameManager.instance.enemyPanelObject.AddComponent<G_FadeIn>();
        //G_GameManager.instance.enemyPanelTypeIcon.AddComponent<G_FadeIn>();
    }


    public void PlayEnemyPanelShake(int duration) // Duration is in milliseconds
    {
        // Set the flag and store some needed info, should be good
        playingShakeAnimationDuration = duration;
        playingShakeAnimation = true;
        playingShakeAnimationStart = DateTime.Now;
    }

    public void PlaySparkAnimation(Vector2 position, int density)
    {
        // Spawn the animation prefabs
        for (int i = 0; i < density; i++)
        {
            GameObject g = Instantiate(G_GameManager.instance.sparkAnimationPrefab) as GameObject;
            g.transform.parent = G_GameManager.instance.canvasRoot.transform;

            // Set it up
            g.GetComponent<UiSparkanimationImage>().SetPosition(position);

            // Should be all we really need to do
        }
    }

    public void PlayLaserAnimation(Vector2 position, int density)
    {
        // Spawn the animation prefabs
        for (int i = 0; i < density; i++)
        {
            GameObject g = Instantiate(G_GameManager.instance.laserAnimationPrefab) as GameObject;
            g.transform.parent = G_GameManager.instance.canvasRoot.transform;

            // Set it up
            g.GetComponent<UiSparkanimationImage>().SetPosition(position);

            // Should be all we really need to do
        }
    }
}
