﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class G_GameManager : MonoBehaviour {

    // Public links
    public G_TypeManager type;
    public Text scoreLabelText;
    public RectTransform canvasBackdrop;
    public GameObject gameOverPanelRoot;
    public GameObject gameOverButton;
    public GameObject typeSelectionObject1;
    public GameObject typeSelectionObject2;
    public GameObject typeSelectionObject3;
    public GameObject typeSelectionObject4;
    public GameObject enemyPanelObject;
    public GameObject enemyPanelTypeIcon;
    [HideInInspector]
    public RectTransform enemyPanelObjectRect;

    public GameObject canvasRoot;
    public GameObject sparkAnimationPrefab;
    public GameObject laserAnimationPrefab;

    // Misc vars
    public Vector3 storedEnemyPanelObjectPosition;
    public bool isGameOver = false;


    // Vars for the manager
    private bool typeHasBeenChosen = false;
    private DateTime typeLastChosenTime = DateTime.Now;
    private bool firstTypeRefresh = false;
    private Vector3 originalGameOverPanelLocation;
    private Vector3 originalGameOverButtonLocation;


    // Singleton setup
    //Here is a private reference only this class can access
    private static G_GameManager _instance;

    //This is the public reference that other classes will use
    public static G_GameManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<G_GameManager>();
            return _instance;
        }
    }

    // Begining of our event hooks

    void Awake()
    {
        type = GetComponent<G_TypeManager>();
    }

	void Start () {
	    // On game start clear there score
        GlobalConfiguration.currentUserScore = 0;

        enemyPanelObjectRect = enemyPanelObject.GetComponent<RectTransform>();
        storedEnemyPanelObjectPosition = enemyPanelObjectRect.position;

        // Store the location then move it far out of the way.
        originalGameOverPanelLocation = gameOverPanelRoot.transform.position;
        Vector3 gi = gameOverPanelRoot.transform.position;
        gi.y = 9000;
        gameOverPanelRoot.transform.position = gi;

        originalGameOverButtonLocation = gameOverButton.transform.position;
        gi = gameOverButton.transform.position;
        gi.y = 9000;
        gameOverButton.transform.position = gi;
	}
	
	// Update is called once per frame
	void Update () {
        // Do the first type refresh first frame after everythings setup
        if (!firstTypeRefresh) { firstTypeRefresh = true; type.RefreshTypes(true); }

        checkScore();
        checkTypes();

        // Check for the android back button, doubles as the escape key as well.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameOver(false);
        }
	}

    // Beginning of the sub update events
    // Handle keeping the score label up to date
    private int checkScoreLastScore = -1;
    void checkScore()
    {
        if (checkScoreLastScore != GlobalConfiguration.currentUserScore)
        {
            // Score change!  Store it for the next change and apply it to the score label
            checkScoreLastScore = GlobalConfiguration.currentUserScore;
            scoreLabelText.text = GlobalConfiguration.currentUserScore.ToString();
        }
    }

    // Watch for type choices then handle the transition and animations
    void checkTypes()
    {
        if (typeHasBeenChosen)
        {
            // Wait out the specified millisecond delay before refreshing types
            if ((DateTime.Now - typeLastChosenTime).TotalMilliseconds >= GlobalConfiguration.typeRefreshDelay)
            {
                // Right before we refresh our types, log the previous results
                G_BackManager.instance.Increment();

                typeHasBeenChosen = false;
                type.RefreshTypes(false);
            }
        }
    }


    // Beginning of Public relations
    public void userTypeSelection(int tid, RectTransform r, int bid)
    {
        if (!isGameOver)
        {
            if (!typeHasBeenChosen)
            {
                // Play an animation where they clicked
                //PlaySparkAnimation(new Vector2(r.position.x, r.position.y), 10);

                if (type.CheckSelection(tid))
                {
                    // Good!, now the choosen animation should have started so we can increment our score, then wait while the type shows the results and refreshes
                    GlobalConfiguration.currentUserScore++;
                    typeHasBeenChosen = true;
                    typeLastChosenTime = DateTime.Now;

                    // Do a fly in animation from the succesfull button as if it hit the enemy
                    G_AnimationManager.instance.PlayButtonFlyIn(bid);

                    SPA.instance.HClip();
                }
                else
                {
                    // Game over!
                    SPA.instance.WClip();
                    GameOver(true); // Since it was when they hit the wrong matchup, show the game over screen
                }
            }
        }
    }


    public void GameOver(bool showGameOver)
    {
        // TODO: Good location for any needed "Cleanup" code before we bug out of the scene.
        gameOverPanelRoot.transform.position = originalGameOverPanelLocation;
        gameOverButton.transform.position = originalGameOverButtonLocation;

        isGameOver = true;

        // Store our score
        ScoresG.instance.AddScore(GlobalConfiguration.currentUserScore);
    }
}
