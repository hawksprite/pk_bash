﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class G_BackManager : MonoBehaviour {

    public GameObject backManagerImage;
    public Image lastEnemyLogo;

    //Here is a private reference only this class can access
    private static G_BackManager _instance;

    //This is the public reference that other classes will use
    public static G_BackManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<G_BackManager>();
            return _instance;
        }
    }

    public List<GameObject> logLinks = new List<GameObject>();
    public List<RectTransform> logLinksR = new List<RectTransform>();
    public List<Vector2> logLinksT = new List<Vector2>();

    Vector2 a1Origin;
    Vector2 a2Origin;
    Vector2 a3Origin;
    Vector2 a4Origin;

    RectTransform a1Rect;
    RectTransform a2Rect;
    RectTransform a3Rect;
    RectTransform a4Rect;

	// Use this for initialization
	void Start () {
        a1Rect = G_GameManager.instance.typeSelectionObject1.GetComponent<RectTransform>();
        a2Rect = G_GameManager.instance.typeSelectionObject2.GetComponent<RectTransform>();
        a3Rect = G_GameManager.instance.typeSelectionObject3.GetComponent<RectTransform>();
        a4Rect = G_GameManager.instance.typeSelectionObject4.GetComponent<RectTransform>();

        a1Origin = new Vector3(a1Rect.anchorMin.y, a1Rect.anchorMax.y);
        a2Origin = new Vector3(a2Rect.anchorMin.y, a2Rect.anchorMax.y);
        a3Origin = new Vector3(a3Rect.anchorMin.y, a3Rect.anchorMax.y);
        a4Origin = new Vector3(a4Rect.anchorMin.y, a4Rect.anchorMax.y);
	}
	
	// Update is called once per frame
	void Update () {
        float lS = 20.0f;


        for (int i = 0; i < logLinksR.Count; i++)
        {
            //logLinksR[i].rect.Set(0, 0, 0, 0);

            // Move towards the target
            Vector2 current = new Vector2(logLinksR[i].anchorMin.y, logLinksR[i].anchorMax.y);

            Vector2 newT = Vector2.Lerp(current, logLinksT[i], Time.smoothDeltaTime * lS);

            logLinksR[i].anchorMin = new Vector2(logLinksR[i].anchorMin.x, newT.x);
            logLinksR[i].anchorMax = new Vector2(logLinksR[i].anchorMax.x, newT.y);
        }


        // Lerp our real button's towards there origin
        
        Vector2 a1New = Vector2.Lerp(new Vector2(a1Rect.anchorMin.y, a1Rect.anchorMax.y), a1Origin, Time.smoothDeltaTime * lS);
        Vector2 a2New = Vector2.Lerp(new Vector2(a2Rect.anchorMin.y, a2Rect.anchorMax.y), a2Origin, Time.smoothDeltaTime * lS);
        Vector2 a3New = Vector2.Lerp(new Vector2(a3Rect.anchorMin.y, a3Rect.anchorMax.y), a3Origin, Time.smoothDeltaTime * lS);
        Vector2 a4New = Vector2.Lerp(new Vector2(a4Rect.anchorMin.y, a4Rect.anchorMax.y), a4Origin, Time.smoothDeltaTime * lS);

        a1Rect.anchorMin = new Vector2(a1Rect.anchorMin.x, a1New.x);
        a2Rect.anchorMin = new Vector2(a2Rect.anchorMin.x, a2New.x);
        a3Rect.anchorMin = new Vector2(a3Rect.anchorMin.x, a3New.x);
        a4Rect.anchorMin = new Vector2(a4Rect.anchorMin.x, a4New.x);

        a1Rect.anchorMax = new Vector2(a1Rect.anchorMax.x, a1New.y);
        a2Rect.anchorMax = new Vector2(a2Rect.anchorMax.x, a2New.y);
        a3Rect.anchorMax = new Vector2(a3Rect.anchorMax.x, a3New.y);
        a4Rect.anchorMax = new Vector2(a4Rect.anchorMax.x, a4New.y);
	}

    public void ShiftA()
    {
        a1Rect.anchorMin += new Vector2(0, -0.28f);
        a2Rect.anchorMin += new Vector2(0, -0.28f);
        a3Rect.anchorMin += new Vector2(0, -0.28f);
        a4Rect.anchorMin += new Vector2(0, -0.28f);

        a1Rect.anchorMax += new Vector2(0, -0.28f);
        a2Rect.anchorMax += new Vector2(0, -0.28f);
        a3Rect.anchorMax += new Vector2(0, -0.28f);
        a4Rect.anchorMax += new Vector2(0, -0.28f);

        //G_GameManager.instance.typeSelectionObject1.GetComponent<RectTransform>().anchorMin += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject2.GetComponent<RectTransform>().anchorMin += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject3.GetComponent<RectTransform>().anchorMin += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject4.GetComponent<RectTransform>().anchorMin += new Vector2(0, -0.4f);

        //G_GameManager.instance.typeSelectionObject1.GetComponent<RectTransform>().anchorMax += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject2.GetComponent<RectTransform>().anchorMax += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject3.GetComponent<RectTransform>().anchorMax += new Vector2(0, -0.4f);
        //G_GameManager.instance.typeSelectionObject4.GetComponent<RectTransform>().anchorMax += new Vector2(0, -0.4f);
    }

    public void Move(Vector2 change)
    {
        for (int i = 0; i < logLinksR.Count; i++)
        {
            logLinksR[i].anchorMin += change;
            logLinksR[i].anchorMax += change;
        }
    }

    public void Increment()
    {
        for (int i = 0; i < logLinks.Count; i++)
        {
            GameObject.Destroy(logLinks[i]);
        }

        logLinks = new List<GameObject>();
        logLinksR = new List<RectTransform>();

        // Create a clone and link to all the current buttons
        cloneButton(1);
        cloneButton(2);
        cloneButton(3);
        cloneButton(4);

        // Move it a bit
        //Move(new Vector2(0, 0.28f));
        ShiftA();

        // Set the old enemy logo
        lastEnemyLogo.sprite = TypeLibrary.instance.Library[G_GameManager.instance.type.currentEnemyTID].sprite;

        Color noc = lastEnemyLogo.color;
        noc.a = 0.09f;
        lastEnemyLogo.color = noc;
    }

    private void cloneButton(int id)
    {
        GameObject c = new GameObject();

        switch (id)
        {
            case 1: c = (GameObject)GameObject.Instantiate(G_GameManager.instance.typeSelectionObject1); break;
            case 2: c = (GameObject)GameObject.Instantiate(G_GameManager.instance.typeSelectionObject2); break;
            case 3: c = (GameObject)GameObject.Instantiate(G_GameManager.instance.typeSelectionObject3); break;
            case 4: c = (GameObject)GameObject.Instantiate(G_GameManager.instance.typeSelectionObject4); break;
        }

        c.GetComponent<RectTransform>().SetParent(G_GameManager.instance.canvasRoot.transform, false);

        Color a = c.GetComponent<Image>().color;
        a.a /= 5.0f;
        c.GetComponent<Image>().color = a;

        // We need to do some prep on our clone
        c.GetComponent<Button>().transition = Selectable.Transition.None;
        c.GetComponent<Button>().interactable = false;

        // Set it in the back
        c.GetComponent<RectTransform>().SetAsFirstSibling();

        // Pull the backdrop just ahead of it
        G_GameManager.instance.canvasBackdrop.SetAsFirstSibling();

        logLinks.Add(c); // Store a link to our new object
        logLinksR.Add(c.GetComponent<RectTransform>()); // Also store a link to the rect transform to save on finding it later
        logLinksT.Add(new Vector2(0.28f, 0.28f) + new Vector2(c.GetComponent<RectTransform>().anchorMin.y, c.GetComponent<RectTransform>().anchorMax.y));
    }
}
