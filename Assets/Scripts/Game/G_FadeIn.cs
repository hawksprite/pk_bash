﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class G_FadeIn : MonoBehaviour {

    Image i;
    Text t;

    bool hasText = true;

    // Use this for initialization
    void Start()
    {
        i = GetComponent<Image>();
        t = GetComponentInChildren<Text>();

        if (!t) { hasText = false; }
    }

    float fadeingAlpha = 0.0f;

    // Update is called once per frame
    void Update()
    {

        fadeingAlpha += GlobalConfiguration.animationFadeSpeed * Time.smoothDeltaTime;
        //SetAlpha(fadeingAlpha);

        if (fadeingAlpha >= 1.0f)
        {
            Destroy(this);
        }


    }

    public void SetAlpha(float v)
    {
        // Button sprite color
        Color c = i.color;
        c.a = v;
        i.color = c;

        if (hasText)
        {
            // Button label color
            Color tc = t.color;
            tc.a = v;
            t.color = tc;
        }

        // Flip the enable's to apply the color for sure (Strange issues with 4.6 I haven't found a good fix for yet)
        i.enabled = false;
        i.enabled = true;
    }
}
