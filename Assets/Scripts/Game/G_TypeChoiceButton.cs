﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class G_TypeChoiceButton : MonoBehaviour {

	//[HideInInspector]
    public int tid = 0;

    public int bid = 0; // The button id for this button

    public void SetupType(int tid)
    {
        this.tid = tid;

        // Setup our button with the given tid
        string newName = TypeLibrary.instance.Library[tid].Name;

        GetComponentInChildren<Text>().text = newName;

        /*
        for (int i = 0; i < transform.childCount; i++)
        {
            Image ci = transform.GetChild(i).GetComponent<Image>();

            if (ci)
            {
                Color p = transform.GetChild(i).GetComponent<Image>().color;
                p.a = 0.0f;
                transform.GetChild(1).GetComponent<Image>().color = p;
                //transform.GetChild(i).GetComponent<Image>().sprite = TypeLibrary.instance.Library[tid].sprite;
            }
        }*/

        Image ii = GetComponent<Image>();

        ii.sprite = TypeLibrary.instance.Library[tid].buttonSprite;

        //i.color = TypeLibrary.instance.Library[tid].baseColor;

        //Color c = i.color;

        //c.r = TypeLibrary.instance.Library[tid].baseColor.r;
        //c.g = TypeLibrary.instance.Library[tid].baseColor.g;
        //c.b = TypeLibrary.instance.Library[tid].baseColor.b;

        //i.color = c;

        ii.enabled = false;
        ii.enabled = true;
    }

    public void Implode()
    {
        Component.Destroy(this);
    }

    public void SetText(string newText)
    {
        GetComponentInChildren<Text>().text = newText;
    }

    public void ButtonPressed()
    {
        G_GameManager.instance.userTypeSelection(tid, GetComponent<RectTransform>(), bid); // Let the GM know that the user has pressed a button and choosen a type
    }
}
