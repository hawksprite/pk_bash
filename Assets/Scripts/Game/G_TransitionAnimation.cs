﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class G_TransitionAnimation : MonoBehaviour {

    // Simple barebones just black to fade in transition script for the game scene, so things can get up and running on load

    Text t;
    Image i;

	// Use this for initialization
	void Start () {
        i = GetComponent<Image>();
        t = GetComponentInChildren<Text>();
        launch = DateTime.Now;
    }

    DateTime launch;
    bool beginTransition = false;
    float alpha = 1.0f;
	// Update is called once per frame
	void Update () {

        if (beginTransition)
        {
            alpha -= (GlobalConfiguration.animationFadeSpeed / 5.0f) * Time.smoothDeltaTime;

            Color c = i.color;
            c.a = alpha;
            i.color = c;

            Color ct = t.color;
            ct.a = alpha;
            t.color = ct;

            if (alpha <= 0.0f)
            {
                GameObject.Destroy(this.gameObject); // Kill out the root game object of the loading block
            }
        }
        else
        {
            if ((DateTime.Now - launch).TotalSeconds >= 1)
            {
                beginTransition = true;
            }
        }
	}
}
