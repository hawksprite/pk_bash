﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoresG : MonoBehaviour {

    private static ScoresG _instance;

    public static ScoresG instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ScoresG>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    public List<int> HighScores = new List<int>();

    public void LoadScores()
    {
        // Clear the current list
        HighScores = new List<int>();

        for (int i = 0; i < 8; i++)
        {
            if (PlayerPrefs.HasKey(i.ToString() + "_score"))
            {
                HighScores.Add(PlayerPrefs.GetInt(i.ToString() + "_score"));
            }
            else
            {
                HighScores.Add(0);
            }
        }
    }

    public void SaveScores()
    {
        for (int i = 0; i < HighScores.Count; i++)
        {
            string key = i.ToString() + "_score";
            PlayerPrefs.SetInt(key, HighScores[i]);
        }
    }

    public void AddScore(int score)
    {
        // Push it into our list
        HighScores.Add(score);

        HighScores.Sort(); // This sorts in the wrong direction
        HighScores.Reverse(); // Just reverse it then we're good

        // Remove whatever was lowest
        HighScores.RemoveAt(HighScores.Count - 1);

        // Save our list
        SaveScores();
    }
}
