﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MM_ScoreManager : MonoBehaviour {

    //Here is a private reference only this class can access
    private static MM_ScoreManager _instance;

    //This is the public reference that other classes will use
    public static MM_ScoreManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<MM_ScoreManager>();
            return _instance;
        }
    }

    public RectTransform b1;
    public RectTransform b2;
    public RectTransform b3;

    public Text scoreSub;

    private RectTransform panel;

    // Some actual configruation
    [HideInInspector]
    public float XShift = 0;
    public float ShiftSpeed = 10.0f;

    // Original locations of stuff
    private Vector2 originalA;
    private Vector2 originalb1;
    private Vector2 originalb2;
    private Vector2 originalb3;

    // Target vectors for the update animation
    private Vector2 targetPA;
    private Vector2 targetb1;
    private Vector2 targetb2;
    private Vector2 targetb3;

	// Use this for initialization
	void Start () {
        XShift = Screen.width * 2; // Safe X shift distance

        panel = GetComponent<RectTransform>();

        ScoresG.instance.LoadScores();

        scoreSub.text = ""; // Clear it

        // Now build our list off there scores
        for (int i = 0; i < 8; i++)
        {
            string v = "--";

            if (ScoresG.instance.HighScores.Count >= i)
            {
                v = ScoresG.instance.HighScores[i].ToString();
            }

            scoreSub.text += (i + 1).ToString() + " - " + v + "\n";
        }

        // Store our original anchors for the buttons, set the targets as well so they're not moving by default
        targetb1 = originalb1 = b1.anchoredPosition;
        targetb2 = originalb2 = b2.anchoredPosition;
        targetb3 = originalb3 = b3.anchoredPosition;

        // Hide our window
        Hide(false);
	}
	
	// Update is called once per frame
	void Update () {
	    // Move our rect transforms toward there targets, lerping for animation

        b1.anchoredPosition = Vector2.Lerp(b1.anchoredPosition, targetb1, Time.smoothDeltaTime * ShiftSpeed);
        b2.anchoredPosition = Vector2.Lerp(b2.anchoredPosition, targetb2, Time.smoothDeltaTime * ShiftSpeed);
        b3.anchoredPosition = Vector2.Lerp(b3.anchoredPosition, targetb3, Time.smoothDeltaTime * ShiftSpeed);

        panel.anchoredPosition = Vector2.Lerp(panel.anchoredPosition, targetPA, Time.smoothDeltaTime * ShiftSpeed);
	}

    public void Hide(bool restoreButtons)
    {
        if (!restoreButtons)
        {
            // Store where our panel is now, because it's visible
            originalA = panel.anchoredPosition;

            // Calculate hidden position
            Vector2 newA = originalA + new Vector2(XShift, 0);

            // Set it
            panel.anchoredPosition = newA;
            targetPA = newA;
        }
        else
        {
            targetPA = originalA + new Vector2(XShift, 0); // Target the panel back off screen

            // Target our buttons back home
            targetb1 = originalb1;
            targetb2 = originalb2;
            targetb3 = originalb3;
        }
    }

    public void Show()
    {
        // Set our new targets
        targetb1 = originalb1 - new Vector2(XShift, 0);
        targetb2 = originalb2 - new Vector2(XShift, 0);
        targetb3 = originalb3 - new Vector2(XShift, 0);

        // Target in the scores panel
        targetPA = originalA;
    }
}
