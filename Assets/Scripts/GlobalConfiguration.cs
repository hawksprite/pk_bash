﻿using UnityEngine;
using System.Collections;

public static class GlobalConfiguration {

    // Configurations
    public static int typeRefreshDelay = 1000; // In milliseconds
    public static bool usePhoneVibration = true;
    public static float enemyPanelShakeIntensity = 10.0f;
    public static float animationFadeSpeed = 10.0f;
    public static float animationFlyInSpeed = 17.0f;

    // Global vars
    public static int currentUserScore = 0;

    // Global utils
    public static string EffectivnessName(Effectivness e)
    {
        switch(e)
        {
            case Effectivness.Immune: return "Immune";
            case Effectivness.Normal: return "1X";
            case Effectivness.Super_Effective: return "2X";
            case Effectivness.Weak: return "1/2X";
        }

        return "";
    }

}
