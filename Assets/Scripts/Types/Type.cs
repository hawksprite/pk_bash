﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Enum for the effectivness level of our matches
public enum Effectivness
{
    Immune = 0,
    Weak = 1,
    Normal = 2,
    Super_Effective = 4
}

// Struct for the type match "Match"
public struct Match
{
    public int tid;
    public Effectivness effectivness;
}

public class Type {

    // Simple holder class for pokemon type's 

    public string Name;
    public Color baseColor;
    public Color textColor;
    public string Icon;
    public Sprite sprite;
    public Sprite buttonSprite;

    public List<Match> Matches = new List<Match>();

    // Type constructor
    public Type(string name, Color color, Sprite s, Sprite bs) { Name = name; baseColor = color; sprite = s; buttonSprite = bs; }

    public void AddMatch(Match newMatch) { Matches.Add(newMatch); }

    // Check effectivness
    public Effectivness CheckMatch(int tid)
    {
        // See if we have a match for that tid
        for (int i = 0; i < Matches.Count; i++)
        {
            if (Matches[i].tid == tid)
            {
                // Found a match!
                return Matches[i].effectivness;
            }
        }

        // Otherwise no luck, return a "Normal" effectivness as the fallback
        return Effectivness.Normal;
    }
}