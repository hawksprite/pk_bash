﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TypeLibrary : MonoBehaviour {

    public List<Type> Library = new List<Type>();

    private List<int> randomTIDHistory = new List<int>();

    private static TypeLibrary _instance;

    public Sprite[] SpriteLibrary;
    public Sprite[] ButtonLibrary;


    public static TypeLibrary instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<TypeLibrary>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Start()
    {
        SetupLibrary();
    }

    private bool hasLibrarySetup = false;
    public void SetupLibrary()
    {
        if (!hasLibrarySetup)
        {
            // Start by creating our framework of all the types
            Library = new List<Type>();

            Library.Add(new Type("Normal",      HC("cecece"), SpriteLibrary[0],    ButtonLibrary[0]));    // 0
            Library.Add(new Type("Fighting",    HC("db4d46"), SpriteLibrary[1],    ButtonLibrary[1]));  // 1
            Library.Add(new Type("Flying",      HC("ccbcfd"), SpriteLibrary[2],    ButtonLibrary[2]));    // 2
            Library.Add(new Type("Poison",      HC("be70be"), SpriteLibrary[3],    ButtonLibrary[3]));    // 3
            Library.Add(new Type("Ground",      HC("f5da8f"), SpriteLibrary[4],    ButtonLibrary[4]));    // 4
            Library.Add(new Type("Rock",        HC("ddc766"), SpriteLibrary[5],    ButtonLibrary[5]));      // 5   
            Library.Add(new Type("Bug",         HC("c6d549"), SpriteLibrary[6],    ButtonLibrary[6]));       // 6
            Library.Add(new Type("Ghost",       HC("a38ec7"), SpriteLibrary[7],    ButtonLibrary[7]));     // 7
            Library.Add(new Type("Steel",       HC("d8d8df"), SpriteLibrary[8],    ButtonLibrary[8]));     // 8
            Library.Add(new Type("Fire",        HC("faa86d"), SpriteLibrary[9],    ButtonLibrary[9]));      // 9    
            Library.Add(new Type("Water",       HC("95b3f9"), SpriteLibrary[10],   ButtonLibrary[10]));     // 10
            Library.Add(new Type("Grass",       HC("89d166"), SpriteLibrary[11],   ButtonLibrary[11]));     // 11
            Library.Add(new Type("Electric",    HC("ffe374"), SpriteLibrary[12],   ButtonLibrary[12]));  // 12
            Library.Add(new Type("Psychic",     HC("f77fa3"), SpriteLibrary[13],   ButtonLibrary[13]));   // 13
            Library.Add(new Type("Ice",         HC("b5eeee"), SpriteLibrary[14],   ButtonLibrary[14]));       // 14
            Library.Add(new Type("Dragon",      HC("986ffe"), SpriteLibrary[15],   ButtonLibrary[15]));    // 15
            Library.Add(new Type("Dark",        HC("978477"), SpriteLibrary[16],   ButtonLibrary[16]));      // 16   
            Library.Add(new Type("Fairy",       HC("f1b7c4"), SpriteLibrary[17],   ButtonLibrary[17]));     // 17

            // Now enter all of there type matchups
            setupLibraryBuildMatchups();

            hasLibrarySetup = false;
        }
    }

    private void setupLibraryBuildMatchups()
    {
        // These matchups are attacks coming from the given library entry to another type.
        // So if you select the library entry for electric.  You'll add a matchup for Type: Ground, Effectivness: Immune.

        // First entry, selected Normal.  Then added a match for tid = 5 (Rock) and it's effectivness is weak.  Since normal moves are weak against rock types.
        Library[0].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Weak });
        Library[0].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Immune });

        Library[1].AddMatch(new Match() { tid = 0, effectivness = Effectivness.Super_Effective });
        Library[1].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Weak });
        Library[1].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Weak });
        Library[1].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Super_Effective });
        Library[1].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Weak });
        Library[1].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Immune });
        Library[1].AddMatch(new Match() { tid = 13, effectivness = Effectivness.Weak });
        Library[1].AddMatch(new Match() { tid = 14, effectivness = Effectivness.Super_Effective });

        //flying
        Library[2].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Super_Effective });
        Library[2].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Weak });
        Library[2].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Super_Effective });
        Library[2].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[2].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Super_Effective });
        Library[2].AddMatch(new Match() { tid = 12, effectivness = Effectivness.Weak });
        //poison
        Library[3].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Weak });
        Library[3].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Weak });
        Library[3].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Weak });
        Library[3].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Weak });
        Library[3].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Immune });
        Library[3].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Super_Effective });
        Library[3].AddMatch(new Match() { tid = 17, effectivness = Effectivness.Super_Effective });
        //ground
        Library[4].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Immune });
        Library[4].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Super_Effective });
        Library[4].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Super_Effective });
        Library[4].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Weak });
        Library[4].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Super_Effective });
        Library[4].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Super_Effective });
        Library[4].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Weak });
        Library[4].AddMatch(new Match() { tid = 12, effectivness = Effectivness.Super_Effective });
        //rock
        Library[5].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Weak });
        Library[5].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Super_Effective });
        Library[5].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Weak });
        Library[5].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Super_Effective });
        Library[5].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[5].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Super_Effective });
        Library[5].AddMatch(new Match() { tid = 14, effectivness = Effectivness.Super_Effective });
        //bug
        Library[6].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Weak });
        Library[6].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Super_Effective });
        Library[6].AddMatch(new Match() { tid = 13, effectivness = Effectivness.Super_Effective });
        Library[6].AddMatch(new Match() { tid = 16, effectivness = Effectivness.Super_Effective });
        Library[6].AddMatch(new Match() { tid = 17, effectivness = Effectivness.Weak });
        //ghost
        Library[7].AddMatch(new Match() { tid = 0, effectivness = Effectivness.Immune });
        Library[7].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Super_Effective });
        Library[7].AddMatch(new Match() { tid = 13, effectivness = Effectivness.Super_Effective });
        Library[7].AddMatch(new Match() { tid = 16, effectivness = Effectivness.Weak });
        //steel
        Library[8].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Super_Effective });
        Library[8].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[8].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Weak });
        Library[8].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Weak });
        Library[8].AddMatch(new Match() { tid = 12, effectivness = Effectivness.Weak });
        Library[8].AddMatch(new Match() { tid = 14, effectivness = Effectivness.Super_Effective });
        Library[8].AddMatch(new Match() { tid = 17, effectivness = Effectivness.Super_Effective });
        //fire
        Library[9].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Weak });
        Library[9].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Super_Effective });
        Library[9].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Super_Effective });
        Library[9].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Weak });
        Library[9].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Weak });
        Library[9].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Super_Effective });
        Library[9].AddMatch(new Match() { tid = 14, effectivness = Effectivness.Super_Effective });
        Library[9].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Weak });
        //water
        Library[10].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Super_Effective });
        Library[10].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Super_Effective });
        Library[10].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Super_Effective });
        Library[10].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Weak });
        Library[10].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Weak });
        Library[10].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Weak });
        //grass (better than electric)
        Library[11].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Super_Effective });
        Library[11].AddMatch(new Match() { tid = 5, effectivness = Effectivness.Super_Effective });
        Library[11].AddMatch(new Match() { tid = 6, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Super_Effective });
        Library[11].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Weak });
        Library[11].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Weak });
        //electric(fuck you justin)
        Library[12].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Super_Effective });
        Library[12].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Immune });
        Library[12].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Super_Effective });
        Library[12].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Weak });
        Library[12].AddMatch(new Match() { tid = 12, effectivness = Effectivness.Weak });
        Library[12].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Weak });
        //psychic
        Library[13].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Super_Effective });
        Library[13].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Super_Effective });
        Library[13].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[13].AddMatch(new Match() { tid = 13, effectivness = Effectivness.Weak });
        Library[13].AddMatch(new Match() { tid = 16, effectivness = Effectivness.Immune });
        //ice
        Library[14].AddMatch(new Match() { tid = 2, effectivness = Effectivness.Super_Effective });
        Library[14].AddMatch(new Match() { tid = 4, effectivness = Effectivness.Super_Effective });
        Library[14].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[14].AddMatch(new Match() { tid = 9, effectivness = Effectivness.Weak });
        Library[14].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Weak });
        Library[14].AddMatch(new Match() { tid = 11, effectivness = Effectivness.Super_Effective });
        Library[14].AddMatch(new Match() { tid = 13, effectivness = Effectivness.Weak });
        Library[14].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Super_Effective });
        //dragon
        Library[15].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[15].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Super_Effective });
        Library[15].AddMatch(new Match() { tid = 17, effectivness = Effectivness.Immune });
        //dark
        Library[16].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Weak });
        Library[16].AddMatch(new Match() { tid = 7, effectivness = Effectivness.Super_Effective });
        Library[16].AddMatch(new Match() { tid = 16, effectivness = Effectivness.Weak });
        Library[16].AddMatch(new Match() { tid = 17, effectivness = Effectivness.Weak });
        //fairy(meta game savior)
        Library[17].AddMatch(new Match() { tid = 1, effectivness = Effectivness.Super_Effective });
        Library[17].AddMatch(new Match() { tid = 3, effectivness = Effectivness.Weak });
        Library[17].AddMatch(new Match() { tid = 8, effectivness = Effectivness.Weak });
        Library[17].AddMatch(new Match() { tid = 10, effectivness = Effectivness.Weak });
        Library[17].AddMatch(new Match() { tid = 15, effectivness = Effectivness.Super_Effective });
        Library[17].AddMatch(new Match() { tid = 16, effectivness = Effectivness.Super_Effective });

        //end of type matchup

    }

    public void ClearRandomHistory()
    {
        randomTIDHistory.Clear();
    }

    // Helper functions
    public int RandomTID
    {
        get
        {
            int attempt = Random.Range(0, Library.Count);

            while (randomTIDHistory.Contains(attempt))
            {
                attempt = Random.Range(0, Library.Count);
            }

            randomTIDHistory.Add(attempt);

            return attempt;
        }
    }

    public int RandomEffectiveAgainst(int tid)
    {
        List<int> possiblePool = new List<int>();

        for (int i = 0; i < Library.Count; i++)
        {
            if (Library[i].CheckMatch(tid) == Effectivness.Super_Effective)
            {
                possiblePool.Add(i);
            }
        }

        List<int> newList = new List<int>();
        // Remove from the pool whats in history
        for (int i = 0; i < possiblePool.Count; i++)
        {
            int indexFromPossible = possiblePool[i];

            // Is this index in the history?
            if (randomTIDHistory.Contains(indexFromPossible)) { }
            else
            {
                // It does not, so we can add it to our newList
                newList.Add(indexFromPossible);
            }
        }

        // If we didn't find one, return a random ID
        if (newList.Count <= 0) { return RandomTID; }

        // Choose an index from our list
        int choice = Random.Range(0, newList.Count);
        int choosenIndex = newList[choice];

        randomTIDHistory.Add(choosenIndex);

        return choosenIndex;

    }

    public int RandomWeakAgainst(int tid)
    {
        List<int> possiblePool = new List<int>();

        for (int i = 0; i < Library.Count; i++)
        {
            if (Library[i].CheckMatch(tid) == Effectivness.Weak || Library[i].CheckMatch(tid) == Effectivness.Immune)
            {
                possiblePool.Add(i);
            }
        }

        List<int> newList = new List<int>();
        // Remove from the pool whats in history
        for (int i = 0; i < possiblePool.Count; i++)
        {
            int indexFromPossible = possiblePool[i];

            // Is this index in the history?
            if (randomTIDHistory.Contains(indexFromPossible)) { }
            else
            {
                // It does not, so we can add it to our newList
                newList.Add(indexFromPossible);
            }
        }

        // If we didn't find one, return a random ID
        if (newList.Count <= 0) { return RandomTID; }

        // Choose an index from our list
        int choice = Random.Range(0, newList.Count);
        int choosenIndex = newList[choice];

        randomTIDHistory.Add(choosenIndex);

        return choosenIndex;

    }

    // Hex to unity color for the library
    Color HC(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
}
